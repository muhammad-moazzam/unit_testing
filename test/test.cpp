#include "gtest/gtest.h"
#include "student.h"
#include "whitelist.h"
#include <iostream>
using namespace std;

//StudentTest Fixture
struct student_fixture : testing::Test
{
    Student *studentPtr;
    student_fixture()
    {
        studentPtr = new Student("125_ab", 30, 2.89);
    }
};

TEST_F(student_fixture, get_rollnum_test)
{
    EXPECT_EQ("125_ab", studentPtr->get_rollnum());
}

TEST_F(student_fixture, get_age_test)
{
    EXPECT_EQ(30, studentPtr->get_age());
}

TEST_F(student_fixture, get_cgpa_test)
{
    ASSERT_FLOAT_EQ(2.89, studentPtr->get_cgpa());
}

//Parameterized Test of Setting and Getting Roll Number
struct student_rollnum
{
    string roll_no;
};

struct roll_num_test : student_fixture, testing::WithParamInterface<student_rollnum>
{
    roll_num_test()
    {
        studentPtr->set_rollnum(GetParam().roll_no);
    }
};

TEST_P(roll_num_test, rollNumberTest)
{
    EXPECT_EQ(GetParam().roll_no, studentPtr->get_rollnum());
}

INSTANTIATE_TEST_CASE_P(Default, roll_num_test,
                        testing::Values(
                            student_rollnum{"abc242zs"},
                            student_rollnum{"5182dws"},
                            student_rollnum{"jb2u2ds92"}));

//Parameterized Test of Setting and Getting Age

struct student_age
{
    int age;
};

struct age_test : student_fixture, testing::WithParamInterface<student_age>
{
    age_test()
    {
        studentPtr->set_age(GetParam().age);
    }
};

TEST_P(age_test, ageTest)
{
    EXPECT_EQ(GetParam().age, studentPtr->get_age());
}

INSTANTIATE_TEST_CASE_P(Default, age_test,
                        testing::Values(
                            student_age{20},
                            student_age{418},
                            student_age{885}));

//Parameterized Test of Setting and Getting cgpa

struct student_cgpa
{
    float cgpa;
};

struct cgpa_test : student_fixture, testing::WithParamInterface<student_cgpa>
{
    cgpa_test()
    {
        studentPtr->set_cgpa(GetParam().cgpa);
    }
};

TEST_P(cgpa_test, cgpaTest)
{
    ASSERT_FLOAT_EQ(GetParam().cgpa, studentPtr->get_cgpa());
}

INSTANTIATE_TEST_CASE_P(Default, cgpa_test,
                        testing::Values(
                            student_cgpa{2.99},
                            student_cgpa{3.57},
                            student_cgpa{1.2}));

//Parameterized Test of Setting and Getting subject Marks

struct student_subject_marks
{
    string subject;
    int marks;
};

struct student_subject_marks_test : student_fixture, testing::WithParamInterface<student_subject_marks>
{
    student_subject_marks_test()
    {
        studentPtr->set_subject_marks(GetParam().subject, GetParam().marks);
    }
};

TEST_P(student_subject_marks_test, SubjectMarksTest)
{
    EXPECT_EQ(GetParam().marks, studentPtr->get_subject_marks(GetParam().subject));
}

INSTANTIATE_TEST_CASE_P(Default, student_subject_marks_test,
                        testing::Values(
                            student_subject_marks{"Maths", 99},
                            student_subject_marks{"Science", 58},
                            student_subject_marks{"English", 62}));

//WHITELIST TESTS

//whitelist_fixture
struct whitelist_fixture : testing::Test
{
    whitelist *wlist;
    Student *stdptr;
    whitelist_fixture()
    {
        wlist = new whitelist();
    }
};

struct add_to_whitelist
{
    string StudentName;
};

struct add_to_whitelist_test : whitelist_fixture, testing::WithParamInterface<add_to_whitelist>
{

    add_to_whitelist_test()
    {
        stdptr = new Student(to_string(rand()), rand(), float(rand()));
        wlist->add_to_whitelist(GetParam().StudentName, *stdptr);
    }
};

TEST_P(add_to_whitelist_test, AddingToWhitelist)
{
    Student *getstd = wlist->get_student_data(GetParam().StudentName);
    EXPECT_EQ(stdptr->get_age(), getstd->get_age());
    EXPECT_EQ(stdptr->get_rollnum(), getstd->get_rollnum());
    EXPECT_EQ(stdptr->get_cgpa(), getstd->get_cgpa());
}

INSTANTIATE_TEST_CASE_P(Default, add_to_whitelist_test,
                        testing::Values(
                            add_to_whitelist{"Ahmed"},
                            add_to_whitelist{"Sohail"},
                            add_to_whitelist{"Akbar"}));

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}