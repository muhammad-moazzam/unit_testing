#include "whitelist.h"
#include <iostream>
using namespace std;

void whitelist::add_to_whitelist(const string &studentName, const Student &student)
{
    if (!is_student_present(studentName))
    {
        student_data_list.push_back(student);
        student_record[studentName] = &student_data_list.back();
    }
    else
    {
        cout << "Student already existed";
    }
}

bool whitelist::is_student_present(const string &studentName) const
{
    if (student_record.find(studentName) == student_record.end())
    {
        return 0;
    }

    else
    {
        return 1;
    }
}

Student *whitelist::get_student_data(const string &studentName) const
{
    auto search = student_record.find(studentName);
    if (search != student_record.end())
    {
        return search->second;
    }
    else
    {
        return nullptr;
    }
}